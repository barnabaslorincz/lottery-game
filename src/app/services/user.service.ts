import { Injectable } from '@angular/core';
import { UserModel } from "../models/login/user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private users: UserModel[] = [
    { username: 'anna', userId: '2dbbb2' },
    { username: 'bela', userId: '1cc765' },
    { username: 'cili', userId: 'e01f91' },
    { username: 'denes', userId: '96f5fc' },
    { username: 'eszter', userId: 'b5fb14' },
  ]

  constructor() {}

  getUsers(): UserModel[] {
    return this.users;
  }

  getUserById(userId: string): UserModel | undefined {
    return this.users.find(user => user.userId === userId);
  }
}



