import { Injectable } from '@angular/core';
import { UserService } from "./user.service";
import { Observable } from "rxjs";
import { FieldErrorModel } from "../models/login/field-error.model";

@Injectable({
  providedIn: 'root'
})
export class MockApiService {
  private readonly MOCK_PASSWORD: string = "password";

  constructor(private userService: UserService) { }

  authenticate(userId: string, password: string): Observable<FieldErrorModel> {
    const userModel = this.userService.getUserById(userId);
    let fieldErrorModel: FieldErrorModel;
    let success = false;
    if (userModel) {
      if (password === this.MOCK_PASSWORD) {
        success = true;
      } else {
        fieldErrorModel = {
          passwordError: 'Invalid password'
        }
      }
    } else {
      fieldErrorModel = {
        userIdError: 'User was not found'
      }
    }

    return new Observable<FieldErrorModel>(subscriber => {
      success ? subscriber.next() : subscriber.error(fieldErrorModel);
    });
  }
}
