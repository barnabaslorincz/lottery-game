import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { AuthenticationService } from "./authentication.service";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}

  canActivate(): boolean {
    if (!this.authenticationService.isLoggedIn) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
