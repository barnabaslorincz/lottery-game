import { RandomService } from "./random.service";

describe('Random Service', () => {

  describe('generateRandom function', () => {
    it('should return random in interval', () => {
      const random: number = RandomService.generateRandom(100);
      expect(random).toBeLessThan(100);
    });
  });

  describe('generateRandomArray function', () => {
    const max: number = 100;
    const numberOfValues: number = 6;
    const randomArray: number[] = RandomService.generateRandomArray(max, numberOfValues);

    it('should return random array with given length', () => {
      expect(randomArray.length).toBe(numberOfValues);
    });
    it('should return random array with numbers in interval', () => {
      for (let i = 0; i < randomArray.length; i++) {
        expect(randomArray[i]).toBeLessThan(max);
      }
    });
  });
});
