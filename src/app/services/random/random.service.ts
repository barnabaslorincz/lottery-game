import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomService {

  constructor() { }

  static generateRandomArray(max: number, numberOfValues: number): number[] {
    const numbers: number[] = [];
    while (numberOfValues > 0) {
      const random = this.generateRandom(max);
      if (numbers.indexOf(random) === -1) {
        numbers.push(random);
        numberOfValues--;
      }
    }
    return numbers;
  }

  static generateRandom(max: number): number {
    return Math.floor(Math.random() * max)
  }
}
