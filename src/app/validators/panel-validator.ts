import { PanelModel } from "../models/game/panel.model";
import { ValidationResultModel } from "../models/game/validation-result.model";

export namespace PanelValidator {

  export function validate(panel: PanelModel): ValidationResultModel {
    let message;
    const selectedNumbers = panel.getSelectedNumbers();
    if (selectedNumbers.length === 0) {
      message = "Empty.";
    }
    else if (selectedNumbers.length < panel.numberOfRequiredSelections) {
      message = `Error: ${6 - selectedNumbers.length} mark(s) missing.`
    }
    else if (selectedNumbers.length > panel.numberOfRequiredSelections) {
      message = `Error: Please remove ${selectedNumbers.length - panel.numberOfRequiredSelections} mark(s).`
    }
    else {
      message = selectedNumbers.join(", ");
    }
    return { message };
  }

}
