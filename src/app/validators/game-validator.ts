import { GameModel } from "../models/game/game.model";
import { ValidationResultModel } from "../models/game/validation-result.model";
import { PanelValidator } from "./panel-validator";

export namespace GameValidator {
  export function validate(game: GameModel): ValidationResultModel[] {
    const validationResult: ValidationResultModel[] = [];
    for (let i = 0; i < game.panels.length; i++) {
      validationResult.push(PanelValidator.validate(game.panels[i]));
    }
    return validationResult;
  }
}
