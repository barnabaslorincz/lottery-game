import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { GameComponent } from "./views/game/game.component";
import { LoginComponent } from "./views/login/login.component";
import { PanelContainerComponent } from "./views/game/panel-container/panel-container.component";
import { ResultComponent } from "./views/game/result/result.component";
import { PanelComponent } from "./views/game/panel-container/panel/panel.component";
import { DeleteButtonComponent } from "./views/game/panel-container/panel/delete-button/delete-button.component";
import { RandomButtonComponent } from "./views/game/panel-container/panel/random-button/random-button.component";
import { NumberFieldComponent } from "./views/game/panel-container/panel/number-field/number-field.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PlayButtonComponent } from './views/game/play-button/play-button.component';
import { IconButtonComponent } from './views/shared/icon-button/icon-button.component';
import { AppRouterModule } from "./app-router.module";
import { UserListComponent } from './views/login/user-list/user-list.component';
import { LoginFormComponent } from "./views/login/login-form/login-form.component";

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    LoginComponent,
    PanelContainerComponent,
    ResultComponent,
    PanelComponent,
    DeleteButtonComponent,
    RandomButtonComponent,
    NumberFieldComponent,
    PlayButtonComponent,
    IconButtonComponent,
    UserListComponent,
    LoginFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRouterModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
