import { IconButtonComponent } from "./icon-button.component";

describe('Icon Button Component', () => {
  let component: IconButtonComponent;
  beforeEach(() => {
    component = new IconButtonComponent();
  });

  it('Text is set correctly', () => {
    component.text = "Test";
    expect(component.text).toBe("Test");
  });

  it('EventEmitter emits when button is clicked', () => {
    spyOn(component.iconButtonEventEmitter, 'emit');
    component.onIconButtonClicked();
    expect(component.iconButtonEventEmitter.emit).toHaveBeenCalled();
  });
});
