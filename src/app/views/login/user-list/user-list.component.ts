import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { UserService } from "../../../services/user.service";
import { UserModel } from "../../../models/login/user.model";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: UserModel[] = this.userService.getUsers();

  @Output()
  userIdEventEmitter: EventEmitter<string> = new EventEmitter<string>();

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  onUserClicked(user: UserModel) {
    this.userIdEventEmitter.emit(user.userId);
  }
}
