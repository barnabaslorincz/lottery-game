import { Component, OnInit } from '@angular/core';
import { FieldErrorModel } from "../../../models/login/field-error.model";
import { MockApiService } from "../../../services/mock-api.service";
import { AuthenticationService } from "../../../services/authentication.service";
import { Router } from "@angular/router";
import { FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  loginForm = this.formBuilder.group({
    userId: [''],
    password: [''],
  });

  get userId() { return this.loginForm.get('userId'); }
  get password() { return this.loginForm.get('password'); }

  fieldErrors: FieldErrorModel = {};

  constructor(
    private mockApiService: MockApiService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.mockApiService.authenticate(this.userId?.value, this.password?.value).subscribe(() => {
        this.authenticationService.isLoggedIn = true;
        this.router.navigate(['game']);
      },
      (fieldErrors: FieldErrorModel) => {
        this.fieldErrors = fieldErrors;
      });
  }

  setUserId($event: string) {
    this.loginForm.controls['userId'].setValue($event);
  }

}
