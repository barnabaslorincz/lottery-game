import { Component, Input, OnInit } from '@angular/core';
import { ValidationResultModel } from "../../../models/game/validation-result.model";

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {

  @Input()
  validationResults: ValidationResultModel[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
