import { Component, OnInit } from '@angular/core';
import { GameModel } from "../../models/game/game.model";
import { ValidationResultModel } from "../../models/game/validation-result.model";
import { GameValidator } from "../../validators/game-validator";
import { environment } from "../../../environments/environment";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  game: GameModel = new GameModel(
    environment.numberOfPanels,
    environment.numberOfNumberFields,
    environment.numberOfRequiredSelections
  );
  validationResults: ValidationResultModel[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  play(): void {
    this.validationResults = GameValidator.validate(this.game);
  }

}
