import { Component, Input, OnInit } from '@angular/core';
import { PanelModel } from "../../../models/game/panel.model";

@Component({
  selector: 'app-panel-container',
  templateUrl: './panel-container.component.html',
  styleUrls: ['./panel-container.component.scss']
})
export class PanelContainerComponent implements OnInit {

  @Input()
  panels: PanelModel[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
