import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-random-button',
  templateUrl: './random-button.component.html',
  styleUrls: ['./random-button.component.scss']
})
export class RandomButtonComponent implements OnInit {

  @Output()
  randomButtonEventEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  onRandomButtonClicked(): void {
    this.randomButtonEventEmitter.emit();
  }

}
