import { Component, Input, OnInit } from '@angular/core';
import { PanelModel } from "../../../../models/game/panel.model";

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  @Input()
  panel: PanelModel = new PanelModel();

  @Input()
  panelIndex: number = 0;

  constructor() {
  }

  ngOnInit(): void {
  }

  onDeleteButtonClicked() {
    this.panel.reset();
  }

  onRandomButtonClicked() {
    this.panel.reset();
    this.panel.selectRandom();
  }
}
