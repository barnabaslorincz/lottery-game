import { Component, Input, OnInit } from '@angular/core';
import { NumberFieldModel } from "../../../../../models/game/number-field.model";

@Component({
  selector: 'app-number-field[numberField]',
  templateUrl: './number-field.component.html',
  styleUrls: ['./number-field.component.scss']
})
export class NumberFieldComponent implements OnInit {

  @Input()
  numberField: NumberFieldModel = {
    value: 0,
    selected: false
  };

  constructor() { }

  ngOnInit(): void {
  }

  toggleFieldSelected() {
    this.numberField.selected = !this.numberField.selected;
  }
}
