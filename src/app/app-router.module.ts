import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from "./views/login/login.component";
import { GameComponent } from "./views/game/game.component";
import { AuthenticationGuardService } from "./services/authentication-guard.service";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'game', component: GameComponent, canActivate: [AuthenticationGuardService] },
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRouterModule { }
