export interface UserModel {
  readonly userId: string;
  readonly username: string;
}
