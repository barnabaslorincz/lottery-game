export interface FieldErrorModel {
  userIdError?: string;
  passwordError?: string;
}
