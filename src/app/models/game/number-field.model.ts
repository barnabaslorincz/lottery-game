export interface NumberFieldModel {
  value: number;
  selected: boolean;
}
