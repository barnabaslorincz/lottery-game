export interface ValidationResultModel {
  readonly message: string;
}
