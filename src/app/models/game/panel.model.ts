import { NumberFieldModel } from "./number-field.model";
import { RandomService } from "../../services/random/random.service";

export class PanelModel {
  private readonly numberOfNumberFields: number;
  readonly numberOfRequiredSelections: number;
  numberFields: NumberFieldModel[] = [];

  constructor(
    numberOfNumberFields: number = 0,
    numberOfRequiredSelections: number = 0
  ) {
    this.numberOfNumberFields = numberOfNumberFields;
    this.numberOfRequiredSelections = numberOfRequiredSelections;
    this.initNumberFields();
  }

  private initNumberFields() {
    for (let i = 0; i < this.numberOfNumberFields; i++) {
      this.numberFields.push({
        value: i+1,
        selected: false
      });
    }
  }

  reset() {
    for (let i = 0; i < this.numberOfNumberFields; i++) {
      this.numberFields[i].selected = false;
    }
  }

  selectRandom() {
    const randomValues: number[] = RandomService.generateRandomArray(
      this.numberOfNumberFields,
      this.numberOfRequiredSelections
    );
    for (let i = 0; i < randomValues.length; i++) {
      this.numberFields[randomValues[i]].selected = true;
    }
  }

  getSelectedNumbers(): number[] {
    return this.numberFields
      .filter(numberField => numberField.selected)
      .map(numberField => numberField.value);
  }
}
