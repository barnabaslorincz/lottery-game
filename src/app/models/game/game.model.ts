import { PanelModel } from "./panel.model";

export class GameModel {
  panels: PanelModel[] = [];

  constructor(
    numberOfPanels: number,
    numberOfNumberFields: number,
    numberOfRequiredSelections: number
  ) {
    this.initPanels(
      numberOfPanels,
      numberOfNumberFields,
      numberOfRequiredSelections
    );
  }

  private initPanels(
    numberOfPanels: number,
    numberOfNumberFields: number,
    numberOfRequiredSelections: number
  ) {
    for (let i = 0; i < numberOfPanels; i++) {
      this.panels.push(new PanelModel(numberOfNumberFields, numberOfRequiredSelections));
    }
  }
}
