export const environment = {
  production: true,
  numberOfPanels: 4,
  numberOfNumberFields: 49,
  numberOfRequiredSelections: 6,
};
