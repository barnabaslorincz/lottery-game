import LoginPage from "../support/page-objects/LoginPage";

describe('Login Form Test', () => {
  let loginPage: LoginPage;
  beforeEach(() => {
    loginPage = new LoginPage();
    cy.visit('/');
  });

  it('Displays login form correctly', () => {
    expect(loginPage.getUserIdInput()).to.exist;
    expect(loginPage.getPasswordInput()).to.exist;
  });
  it('Shows user id feedback with wrong user id', () => {
    loginPage.getLoginButton().click();
    expect(loginPage.getIdFeedback().contains("User was not found"));
  });
  it('Shows password feedback with wrong password', () => {
    loginPage.getUserIdInput().type("2dbbb2");
    loginPage.getLoginButton().click();
    expect(loginPage.getPasswordFeedback().contains("Invalid password"));
  });
  it('Navigate to game page with correct credentials', () => {
    loginPage.getUserIdInput().type("2dbbb2");
    loginPage.getPasswordInput().type("password");
    loginPage.getLoginButton().click();
    cy.location('pathname', {timeout: 1000})
      .should('include', '/game');
  });
})
