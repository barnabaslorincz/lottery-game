class LoginPage {
  getUserIdInput() {
    return cy.get('#user_id');
  }
  getIdFeedback() {
    return cy.get('#user_id_feedback');
  }
  getPasswordInput() {
    return cy.get('#password');
  }
  getPasswordFeedback() {
    return cy.get('#password_feedback');
  }
  getLoginButton() {
    return cy.get('#login_button');
  }
}
export default LoginPage
